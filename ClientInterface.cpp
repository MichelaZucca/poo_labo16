/* 
 * File:   ClientInterface.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on June 1, 2017
 */

#include <algorithm>
#include <stdexcept>

#include "ClientInterface.h"

const string ClientInterface::AFFICHER = "p",
        ClientInterface::EMBARQUER = "e",
        ClientInterface::DEBARQUER = "d",
        ClientInterface::DEPLACER = "m",
        ClientInterface::REINITIALISER = "r",
        ClientInterface::QUITTER = "q",
        ClientInterface::MENU = "h";

ClientInterface::ClientInterface(Controller* controller) :
controller(controller),
commandCount(0) {
}

ClientInterface::~ClientInterface() {
}

string ClientInterface::readCommand() {
    string input;
    if (not(cin >> input)) {
        cin.clear();
        cin.ignore(numeric_limits<int>::max(), '\n');
        throw runtime_error("");
    }
    return input;
}

void ClientInterface::startUserInput() {
    showMenu();
    controller->display();

    while (true) {
        cout << commandCount++ << "> ";
        string cmd;
        try {
            cmd = readCommand();
        } catch (runtime_error& e) {
            displayError("Lecture de la commande impossible.");
            continue;
        }

        if (cmd == AFFICHER) {
            controller->display();
        } else if (cmd == EMBARQUER) {
            string name;
            try {
                name = readCommand();
            } catch (runtime_error& e) {
                displayError("Lecture de la commande impossible.");
                continue;
            }
            controller->addBoat(name);
        } else if (cmd == DEBARQUER) {
            string name;
            try {
                name = readCommand();
            } catch (runtime_error& e) {
                displayError("Lecture de la commande impossible.");
                continue;
            }
            controller->remBoat(name);

        } else if (cmd == DEPLACER) {
            controller->moveBoat();
        } else if (cmd == REINITIALISER) {
            controller->clear();
        } else if (cmd == QUITTER) {
            cin.clear();
            break;
        } else if (cmd == MENU) {
            showMenu();
        } else {
            displayError("Commande inconnue.");
        }

        cin.clear();
    }
}

void ClientInterface::displayError(const string& msg) {
    cout << "Erreur: " << msg << endl;
    cout << "Veuillez recommencer s'il-vous-plait." << endl;
}

/**
 * Afficher le menu du controller
 */
void ClientInterface::showMenu() {
    cout << "p       : afficher" << endl
            << "e <nom> : embarquer <nom> " << endl
            << "d <nom> : debarquer <nom> " << endl
            << "m       : deplacer bateau" << endl
            << "r       : reinitialiser " << endl
            << "q       : quitter" << endl
            << "h       : menu" << endl;
}


