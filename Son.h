/* 
 * File:   Son.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:06
 */

#ifndef SON_H
#define	SON_H
#include <string>
#include "Children.h"

using namespace std;

/**
 * Classe représentant un fils.
 */
class Son : public Children{

   public:
      /**
       * Constructeur d'un fils.
       * @param name le nom
       * @param father le père
       * @param mother la mère
       */
      Son(string name, Person* father, Person* mother);
      virtual ~Son() {} 
      
      /**
       * Redéfinition des contraintes d'un fils. Un fils ne peut pas se trouver 
       * dans une liste avec sa mère sans son père.
       * @param list lsite de personnes où se trouve le fils.
       * @param msg l'éventuel message d'erreur
       * @return true si le fils ne setrouve pas avec sa mère sans son père.
       */
      virtual bool constraint(list<Person*> list, string* msg);
};


#endif	/* SON_H */

