/* 
 * File:   Container.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 5. mai 2017, 14:07
 */
#include "Container.h"
#include <algorithm>    // std::find

/**
 * Constructeur. Permet de définir le nom d'u container.
 * @param name le nom du container
 */
Container::Container(const string& name) : _name(name) {}

Container::~Container() {
    
}

/**
 * Ajoute une personne dans le container.
 * @param p un pointeur vers la personne à ajouter.
 */
bool Container::addPerson(Person* p) {
    _personList.push_back(p);
    return true;
}

/**
 * Retirer une personne du container
 * @param p personne à retirer
 * @return faux si la personne n'est pas dans le container
 */
bool Container::removePerson(Person* p) {
   if(find(_personList.begin(),_personList.end(), p) == _personList.end()){
      return false;
   }
   
   _personList.remove(p);
   return true;
}

/**
 * Afficher le container dans la console.
 */
void Container::display() {
   cout << "< ";
   for(Person* p : _personList){
      cout << p->getName() << " ";
   }
   cout << ">"<< endl;
}

bool Container::isValidState(string* msg){
   // Vérifie l'état des contraintes pour chaque personne
   for(Person* p : _personList){
      // Si une personne n'est pas ok retourner false
      if(!p->constraint(Container::_personList, msg)){
         
         return false;
      }
   }
   return true;
}