/* 
 * File:   ClientInterface.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on June 1, 2017
 */

#ifndef CLIENTINTERFACE_H
#define CLIENTINTERFACE_H

#include "Controller.h"
#include <string>

/**
 * Interface de communication et de saisie console avec l'utilisateur du programme.
 * Cette classe se charge de lire les commandes utilisateurs et d'appeler les méthodes
 * correspondantes d'un Controller.
 */
class ClientInterface {
   // Le Controller à manipuler
   Controller* controller;
   // Nombre de commandes effectuées
   unsigned commandCount;

   // Liste des commandes
   static const string
   AFFICHER,
   EMBARQUER,
   DEBARQUER,
   DEPLACER,
   REINITIALISER,
   QUITTER,
   MENU;
   
   /**
    * Lire une commande de l'utilisateur. Lève une exception de type runtime_error
    * si la saisie n'a pas pu être effectuée. La commande correspond à un mot.
    * @return la commande lue
    */
   string readCommand();
   
   /**
    * Affiche un message d'erreur à l'utilisateur.
    * @param msg le message d'erreur
    */
   void displayError(const string& msg);
   void showMenu();


public:
   /**
    * Constructeur d'une interface.
    * @param controller le controller à manipuler
    */
   ClientInterface(Controller* controller);
   virtual ~ClientInterface();
   
   /**
    * Commencer la lecture de l'input utilisateur. Cette méthode 
    * lit les commandes de l'utilisateur et manipule le controller
    * tant que celui-ci n'a pas entrée la commande QUITTER.
    */
   void startUserInput();
};

#endif /* CLIENTINTERFACE_H */

