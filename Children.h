/* 
 * File:   Children.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:04
 */

#ifndef CHILDREN_H
#define	CHILDREN_H

#include <string>
#include "Person.h"

/**
 * Classe représentant un enfant. Elle hérite de Person. Cette classe ne contient
 * pas de classe .cpp car elle est très petite.
 */
class Children : public Person{
   // Le père de l'enfant
   Person* father;
   
   // La mère de l'enfant
   Person* mother;
   
   public:
      /**
       * Constructeur d'un Children. 
       * @param name le nom de l'enfant
       * @param father le père de l'enfant
       * @param mother la mère de l'enfant
       */
      Children(string name, Person* father, Person* mother): Person(name), father(father), mother(mother){}
      
      /**
       * Détermine si un enfant peut conduire. Cette méthode retourne false.
       * @return false
       */
      virtual bool canDrive() override {
         return false;
      }
      
      /**
       * Détermine les contraintes de validité à l'intérieur d'une liste de Person
       * pour un enfant. Cette méthode est abstraite.
       * @param list la liste de personne à vérifier
       * @param ms le message que l'enfant peut donner en cas d'invalidité
       * @return true si l'enfant est dans une situation valide dans la liste, 
       * false sinon
       */
      virtual bool constraint(list<Person*> list, string* ms) override = 0;
      
      /**
       * Récupérer le père de l'enfant.
       * @return le père
       */
      Person* getFather(){return father; } 
      
      /**
       * Récupérer la mère de l'enfant.
       * @return la mère
       */
      Person* getMother(){return mother; }
};

#endif	/* CHILDREN_H */

