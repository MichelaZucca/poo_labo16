/* 
 * File:   Parent.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:04
 */

#ifndef ADULT_H
#define	ADULT_H
#include <string>
#include "Person.h"

using namespace std;

/**
 * Classe représentant un parent. Un parent peut conduire et n'a pas de 
 * contraintes particulières.
 */
class Parent : public Person{

   public:
      /**
       * Constructeur d'un parent. 
       * @param name le nom 
       */
      Parent(string name);
      virtual ~Parent() {}
      
      /**
       * Détermine si un parent peut conduire. 
       * @return true par défaut
       */
      virtual bool canDrive();

      /**
       * Les contraintes d'un parent. Par défaut un parent peut se trouver 
       * n'importe où.
       * @param list la liste de personnes où se trouve le parent
       * @param msg l'éventuelle message d'erreur 
       * @return true par défaut
       */
      virtual bool constraint(list<Person*> list, string* msg);
};


#endif	/* ADULT_H */

