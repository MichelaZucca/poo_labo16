/* 
 * File:   Cop.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:05
 */

#include "Cop.h"

Cop::Cop(string name) : Person(name){}

bool Cop::canDrive(){
   return true;
}

bool Cop::constraint(list<Person*> list,string* msg){
   return true;
}