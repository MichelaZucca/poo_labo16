/* 
 * File:   Person.h
 * Author: Mathieu Monterverder et Michela Zucca
 *
 * Created on 5. mai 2017, 14:04
 */
#ifndef PERSON_H
#define PERSON_H
#include <string>
#include <iostream>
#include <list>

using namespace std;

/**
 * Classe abstraite représentant une personne. Une personne possède un nom, définit 
 * si elle peut conduire le bateau et également les contraintes régissant son 
 * apparetenance à une liste (si elle a le droit de se trouver avec les personnes
 * dans une liste).
 * 
 * Le nom d'une personne est utilisé comme identifiant unique d'une personne, 
 * et ce en raison du problème simulé. En effet le nom doit permettre d'identifier
 * une personne de manière unique puisque c'est ainsi que l'utilisateur
 * indique les personnes à manipuler.
 */
class Person {
   // Le nom de la personne
   string _name;
public:

   /**
    * Constructeur de la personne.
    * @param name le nom
    */
   Person(string name) : _name(name) {
   }

   /**
    * Afficher une personnes
    */
   virtual void display() {
      cout << _name;
   }

   /**
    * Définit si une personne peut conduire
    * @return true si elle a le droit de conduire, false sinon
    */
   virtual bool canDrive() = 0;

   /**
    * Détermine si une personne a le droit de se trouver dans une liste de 
    * personnes en fonction de ses contraintes. Chaque type de personne définit
    * lui-même ses contraintes, et teste si la liste dans laquelle il se trouve
    * ne brise pas ces contraintes.
    * @param list la liste où se trouve la personne
    * @param msg l'éventuelle message d'erreur
    * @return true si les contraintes ne sont pas enfreintes, false sinon
    */
   virtual bool constraint(list<Person*> list, string* msg) = 0;

   /**
    * Récupérer le nom d'une personne
    * @return le nom d'une personne
    */
   string getName() const {
      return _name;
   }

   /**
    * Surcharge de l'opérateur d'égalité. Deux personnes sont considérées 
    * égales si elles ont le même nom
    * @param p autre personne
    * @return return true si les deux personnes ont le même nom
    */
   virtual bool operator==(const Person& p) {
      return _name.compare(p.getName()) == 0;
   }
};

#endif /* PERSON_H */

