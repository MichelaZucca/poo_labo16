/* 
 * File:   Bank.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 5. mai 2017, 14:15
 */

#ifndef BANK_H
#define BANK_H
#include <string>
#include "Container.h"

using namespace std;

/**
 * Classe définissant une rive. Elle hérite de Container et se son comportement
 * par défaut.
 */
class Bank : public Container {
public:
   /**
    * Construit un objet de type Bank en appelant le constructeur de Container.
    * @param name le nom de la rive
    */
   Bank(const string& name);
};


#endif /* BANK_H */

