/* 
 * File:   Daughter.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:06
 */

#ifndef DAUGHTER_H
#define	DAUGHTER_H
#include <string>
#include "Children.h"

using namespace std;

/**
 * Classe représentant une fille. 
 */
class Daughter : public Children{

   public:
      /**
       * Constructeur d'une fille.
       * @param name le nom de la fille
       * @param father le père
       * @param mother la mère
       */
      Daughter(string name, Person* father, Person* mother);
      virtual ~Daughter() {}
      
      /**
       * Contraintes d'une fille pour se trouver dans une liste de personnes. 
       * Une fille peut se trouver dans n'import quelle liste, tant qu'elle n'est
       * pas avec son père sans sa mère.
       * @param list la liste de personne où se trouve la fille
       * @param msg l'éventuel message d'erreur
       * @return true si la fille n'est pas avec son père sans sa mère
       */
      virtual bool constraint(list<Person*> list, string* msg);
};


#endif	/* DAUGHTER_H */

