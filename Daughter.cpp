/* 
 * File:   Daughter.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:06
 */

#include "Daughter.h"

Daughter::Daughter(string name, Person* father, Person* mother) 
   : Children(name,father,mother){}

// Contrainte de la fille, ne peut pas rester seule avec son père si sa mère n'est pas la
bool Daughter::constraint(list<Person*> list, string* msg){
   bool fatherPresent = false;
   bool motherPresent = false;
   bool validate = true;
   for(Person* p : list){
      if(p == Children::getFather()){
         fatherPresent = true;
      }else if(p == Children::getMother()){
         motherPresent = true;
      }
   }
   if(fatherPresent && !motherPresent){
      validate = false;
      *msg += "\n### fille seule avec son pere sans sa mere";
   }
   
   return validate;
}