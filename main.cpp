/* 
 * File:   main.cpp
 * Author: User
 *
 * Created on 5. mai 2017, 13:48
 */


#include "Bank.h"
#include "Boat.h"
#include "Container.h"
#include "Controller.h"
#include "Person.h"
#include "Cop.h"
#include "Thief.h"
#include "Parent.h"
#include "Children.h"
#include "Daughter.h"
#include "Son.h"
#include "ClientInterface.h"

#include <cstdlib>

using namespace std;

void testConstraint(Controller& controller, 
        Parent* father,
        Parent* mother,
        Son* son1,
        Son* son2,
        Daughter* daug1,
        Daughter* daug2,
        Cop* cop,
        Thief* thief){
   
   controller.display();
   
   
   

   cout << "\n****** Test contrainte Voleur : \nLe voleur ne peut pas rester seul "<<
                       "avec un membre de la famille\nsi le policier est absent\n\n" 
        << " - Policier monte dans le bateau et laisse voleur avec la famille"
        << endl;
   controller.addBoat(cop->getName());
   
   cout << "\n- Voleur monte sur le bateau, policier le rejoint ensuite" << endl;
   controller.addBoat(thief->getName());
   controller.addBoat(cop->getName());
   controller.clear();

    cout << "\n****** Test contrainte Fille : \nUne fille ne peut pas rester seule "
         << "avec son pere sans sa mere" << endl
         << "\n - Fille se retrouve seul avec son pere sans sa mere" 
         << endl;
    
   controller.addBoat(mother->getName());
   controller.clear();
   
    cout << "\n****** Test contrainte Garcon : \nUn garon ne peut pas rester seul "
         << "avec sa mere sans son pere" << endl
         << "\n - Garcon se retrouve seul avec sa mere sans son pere" 
         << endl;
    controller.addBoat(father->getName());
    controller.clear();
   
    
    cout << "\n****** Test contrainte Bateau : "
         << "\nLe bateau se déplace seulement si quelqu'un peut le conduire" << endl
         << "\n - 1 enfant a bord"
         << endl;
    controller.addBoat(son1->getName());
    controller.moveBoat();
    
    cout << "\n - 2 enfants a bord";
    controller.addBoat(daug1->getName());
    controller.moveBoat();
    controller.clear();
    
    cout <<"\n- Thief seul a bord";
    controller.addBoat(thief->getName());
    controller.moveBoat();
    controller.clear();
    
    cout <<"\n - Personne interdite de conduite avec personne autorise a conduire"
         << endl;
    controller.addBoat(thief->getName());
    controller.addBoat(cop->getName());
    controller.moveBoat();
    controller.clear();
    
    cout << "\n****** Test Bateau : "<< endl;
    cout << "\n- Ajouter du monde sur le bateau, test dépassement >2 personnes"<<endl;
    controller.addBoat(thief->getName());
    controller.addBoat(cop->getName());
    controller.addBoat(daug1->getName());
    
    cout << "\n- Deplacer le bateau sur l'autre rive " << endl;
    controller.moveBoat();
    
    cout << "\n- Débarquer du bateau les passagers" << endl;
    controller.remBoat(thief->getName());
    controller.remBoat(cop->getName());
    controller.remBoat(daug1->getName());
}

int main(int argc, char** argv) {

   Parent* father = new Parent("pere");
   Parent* mother = new Parent("mere");
   Son* son1 = new Son("paul", father, mother);
   Son* son2 = new Son("pierre", father, mother);
   Daughter* daug1 = new Daughter("julie", father, mother);
   Daughter* daug2 = new Daughter("jeanne", father, mother);
   Cop* cop = new Cop("policier");
   Thief* thief = new Thief("voleur", cop);

   Controller controller({father, mother, son1, son2, daug1, daug2, cop, thief});

   // Décommenter pour lancer les tests
   //testConstraint(controller, father, mother, son1, son2, daug1, daug2, cop, thief);
   
   ClientInterface ci(&controller);
   ci.startUserInput();

   
   delete father;
   delete mother;
   delete son1;
   delete son2;
   delete daug1;
   delete daug2;
   delete thief;
   delete cop;


   return EXIT_SUCCESS;

}