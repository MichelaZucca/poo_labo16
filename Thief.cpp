/* 
 * File:   Thief.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:05
 */

#include "Thief.h"

Thief::Thief(string name, Cop* cop):Person(name), cop(cop){}

bool Thief::canDrive(){
   return false;
}
 
// Ne peut pas se retrouver avec qqun d'autre sauf si le policier est présent
bool Thief::constraint(list<Person*> list, string* msg){
   bool copPresent = false;
   bool validate = true;
   for(Person* p : list){
      if(p == cop){
         copPresent = true;
      }
   }
   if(!copPresent && list.size() > 1){
      validate = false;
      *msg += "\n### voleur seul avec un membre de la famille sans le policier";
   }
     
   return validate;
}