/* 
 * File:   Boat.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 5. mai 2017, 14:10
 */

#ifndef BOAT_H
#define	BOAT_H
#include <string>
#include "Bank.h"

using namespace std;

/**
 * Définit une classe représentant un bateau. Elle spécialise le comportement
 * par défaut d'un Container.
 */
class Boat : public Container {
   // La rive actuelle où se trouve le bateau
   Bank* _current;
   // Nombre max de personnes que le bateau peut contenir
   unsigned int maxPerson = 2;
   // Nombre de personnes se trouvant dans le abteau à un instant donné
   unsigned int numberPerson = 0;
   
public:
   /**
    * Constructeur d'un bateau. Permet de spécifier le nom du bateau et la 
    * rive où il se trouve à la construction.
    * @param name le nom du bateau
    * @param current la rive sur lequel se trouve le bateau
    */
   Boat(const string& name, Bank* current);
   
   /**
    * Ajoute une personne au bateau. 
    * @param p la personne à ajouter au bateau
    * @return true si il la personne a pu être ajouté et false si il n'y avait 
    * pas la place de le faire.
    */
   virtual bool addPerson(Person* p);
   
   /**
    * Enlever une personne du bateau. 
    * @param p la personne à enlever du bateau.
    * @return true si la personne a pu être enlevée du bateau, false si ce n'est
    * pas le cas, par exemple si la personne n'est pas présente dans le bateau.
    */
   virtual bool removePerson(Person* p);
   
   /**
    * Déplace le bateau sur une rive. Il faut que le bateau contienne au moins 
    * une personne qui puisse conduire le bateau.
    * @param bank la rive vers laquelle déplacer le bateau
    * @return true si le bateau a pu être déplacé, false sinon
    */
   virtual bool move(Bank* bank);
   
   /**
    * Récupérer la rive actuell.
    * @return la rive sur laquelle se trouve le bateau
    */
   virtual Bank* getBank();
   
   /**
    * Vide le bateau.
    * @param bank la rive vers laquelle remettre le bateau.
    */
   virtual void clear(Bank* bank);
   
   /**
    * Déterminer s'il reste de la place sur le bateau.
    * @return true si le bateau n'est pas plein.
    */
   virtual bool placeAvailable()const;
};


#endif	/* BOAT_H */

