/* 
 * File:   Controller.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 5. mai 2017, 14:06
 */

#ifndef CONTROLLER_H
#define	CONTROLLER_H
#include <list>
#include "Boat.h"

/**
 * Classe qui contrôle le comportement de la simulation de ce laboratoire.
 */
class Controller{
   // Liste des personnes gérées
   list<Person*> _personList;
   // La première rive
   Bank* _first;
   
   // La seconde rive
   Bank* _second;
   
   // Le bateau
   Boat* _boat;
   
   /**
    * Retrouver une personne dans la liste par son nom.
    * @param name le nom de la personne
    * @return un poineur vers l'objet Person correspondant dans la liste 
    */
   Person* getPerson(const string& name);
   
public:
   /**
    * Constructeur. Permet de passer une liste de personnes.
    * @param persons les personnes à gérer
    */
   Controller(initializer_list<Person*> persons);
   ~Controller();
   
   /**
    * Afficher l'état de la partie
    */
   void display();
   
   /**
    * Prochain tour. Affiche l'état du tour précédent.
    * @param action l'action effectuée
    * @param error si il y a eu une erreur lors du tour précédent
    */
   void nextTurn(const string& action, bool error);
   
   /**
    * Embarquer un passager sur le bateau depuis la ribe où se trouve 
    * actuellement le bateau
    * @param personName le nom du passager à embarquer
    */
   void addBoat(const string& personName);
   
   /**
    * Débarquer un passager du bateau sur la rive où se trouve actuellement le
    * bateau
    * @param personName le nom du passager à débarquer
    */
   void remBoat(const string& personName);
   
   /**
    * Déplacer le bateau vers l'autre rive.
    */
   void moveBoat();
   
   /**
    * Recommencer la simulation
    */
   void clear();
};


#endif	/* CONTROLLER_H */

