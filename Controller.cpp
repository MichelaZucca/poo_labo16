/* 
 * File:   Controller.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 5. mai 2017, 14:06
 */
#include <stdlib.h>

#include "Controller.h"

/**
 * Initialise une nouvelle instance de Controller. Le Controller se charge
 * de créer les deux rives, et le bateau,
 */
Controller::Controller(initializer_list<Person*> persons)
: _first(new Bank("Gauche")), _second(new Bank("Droite")), _boat(new Boat("Bateau", _first)) {
   for (Person* p : persons) {
      _personList.push_back(p);
      _first->addPerson(p);
   }
}

/**
 * Destructeur. Désalloue la mémoire générée au cours de la vie de l'objet.
 */
Controller::~Controller() {
   delete _first;
   delete _second;
   delete _boat;
}

Person * Controller::getPerson(const string& name) {
   for (Person* p : _personList) {
      if (p->getName() == name) {
         return p;
      }
   }

   return nullptr;
}

/**
 * Afficher l'état du controller. 
 */
void Controller::display() {
   cout << "-------------------------------------------------------------------"
           << endl;
   cout << "Gauche : ";
   _first->display();
   cout << "-------------------------------------------------------------------"
           << endl;
   if (_boat->getBank() == _first) {
      cout << "Boat : ";
      _boat->display();
   } else {
      cout << endl;
   }
   cout << "==================================================================="
           << endl;
   if (_boat->getBank() == _second) {
      cout << "Boat : ";
      _boat->display();
   } else {
      cout << endl;
   }
   cout << "-------------------------------------------------------------------"
           << endl;
   cout << "Droite : ";
   _second->display();
   cout << "-------------------------------------------------------------------"
           << endl;
}

/**
 * Appel au prochain tour de la simulation
 */
void Controller::nextTurn(const string& action, bool error) {
   cout << endl << action << endl;
   if (!error){
      display();
   }
}


void Controller::addBoat(const string& personName) {

   Person* p = getPerson(personName);
   if (p == nullptr) {
      return;
   }

   string msg("e <"+p->getName()+">");
   bool validate = false;
   // Vérifie si il reste de la place sur le bateau, maximum 2 personnes.
   if (_boat->placeAvailable()) {
      // Vérifie si la personne est bien sur la rive pour la retirée
      if (_boat->getBank()->removePerson(p)) {
         // Vérifie les contraintes sans cette personne sur la rive
         if (_boat->getBank()->isValidState(&msg)) {
            // Ajoute la personne dans le bateau
            if (_boat->addPerson(p)) {
               // Vérifie les contraintes sur le bateau
               if (_boat->isValidState(&msg)) {
                  validate = true;
               }
            }
         }
      }
      // Déplacement à échouer, rétablir l'état initial
      if (!validate) {
         _boat->removePerson(p);
         _boat->getBank()->addPerson(p);
      }
      // Pas de place sur le bateau
   } else {
      msg += "\n### Le bateau est plein et ne peut pas embarquer d'autres passagers";
   }

   // Affiche l'état du tour
   nextTurn(msg,!validate);
}

void Controller::moveBoat() {
   string msg = "m ";
   if (_boat->move((_boat->getBank() == _first ? _second : _first))) {
      nextTurn(msg,false);
   } else {
      nextTurn(msg+"\n### Personne ne peut conduire le bateau",true);
   }
}

void Controller::remBoat(const string& personName) {

   Person* p = getPerson(personName);
   if (p == nullptr) {
      return;
   }

   // Personne de la rive du bateau n'existe pas
   string msg("d <"+p->getName()+">");
   bool validate = false;
   if (_boat->removePerson(p)) {
      // Etat de la rive sans la personne
      if (_boat->isValidState(&msg)) {
         // Peut quitter le bateau
         if (_boat->getBank()->addPerson(p)) {
            if (_boat->getBank()->isValidState(&msg)) {
               validate = true;
            }
         }
      }

      // Déplacement pas possible rétablir l'état de base
      if (!validate) {
         _boat->removePerson(p);
         _boat->getBank()->addPerson(p);
      } 
   }else{
         msg+= "\n### "+p->getName()+" n'est pas sur le bateau";
      }
   nextTurn(msg,!validate);
}

void Controller::clear() {
   _first->clear();
   _second->clear();
   _boat->clear(_first);


   for (Person* p : _personList) {
      _first->addPerson(p);
   }
  
   // Error mis à true, pour ne pas afficher l'état de la partie
   nextTurn("c clear",true);
}