/* 
 * File:   Container.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 5. mai 2017, 14:07
 */

#ifndef CONTAINER_H
#define	CONTAINER_H
#include <string>
#include <list>

#include "Person.h"

using namespace std;

/**
 * Classe représentant un container pouvant contenir une liste de personne. 
 * 
 * Un Container peut ajouter, retirer des personnes de sa liste, et peut 
 * également valider son état en demandant à chaque personne si elle est
 * à sa place dans le container au niveau de ses contraintes définires.
 */
class Container{
   // Nom du Container
   string _name;
   // Personnes contenues dans le Container
   list<Person*> _personList;
   
public:
   /**
    * Construit un Container.
    * @param name le nom du container
    */
   Container(const string& name);
   virtual ~Container();
   
   /**
    * Ajouter une personne au container.
    * @param p la personne à ajouter.
    * @return true si la personne a pu être ajoutée. En principe, la personne
    * est toujours ajoutée sans problème.
    */
   virtual bool addPerson(Person* p);
   
   /**
    * Retirer une personne du container.
    * @param p la personne à retirer.
    * @return true si la personne a pu être retirée, false sinon, si par exemple
    * elle n'est pas présente dans le container.
    */
   virtual bool removePerson(Person* p);
   
   /**
    * Afficher le container.
    */
   virtual void display();
   
   /**
    * Valider l'état du container. Valide l'état de chacune des personnes présentes
    * dans le container.
    * @param msg un message qui pourra être transmis par le container quand il y
    * a une erreur
    * @return true si toutes les personnes sont à leur place, false sinon
    */
   virtual bool isValidState(string* msg);
   
   /**
    * Retourne la liste des personnes présentes dans le controller.
    * @return la liste de personnes présentes dans le container
    */
   virtual list<Person*> getList(){return _personList;}
   
   /**
    * Vider la liste de personnes du container.
    */
   virtual void clear(){
      _personList.clear();
   }
};

#endif	/* CONTAINER_H */

