/* 
 * File:   Cop.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:05
 */

#ifndef COP_H
#define	COP_H
#include <string>
#include "Person.h"

using namespace std;

/**
 * Classe qui représente un Policier.
 */
class Cop : public Person {
   
public:
   /**
    * Constructeur d'un policier
    * @param name le nom du policier
    */
   Cop(string name);
   virtual ~Cop() {}

   /**
    * Retourne si le policier peut conduire
    * @return true par défaut
    */
   virtual bool canDrive();

   /**
    * Contraintes du policier pour se trouver avec une liste de personnes.
    * @param list la liste de personnes où se trouve le policier.
    * @param msg le message du policier en cas d'erreur
    * @return true par défaut
    */
   virtual bool constraint(list<Person*> list, string* msg);
};

#endif	/* COP_H */

