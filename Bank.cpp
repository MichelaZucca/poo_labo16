/* 
 * File:   Bank.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 5. mai 2017, 14:15
 */

#include "Bank.h"

/**
 * Constructor
 * @param name
 */
Bank::Bank(const string& name) : Container(name) {}