/* 
 * File:   Son.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:06
 */
#include "Son.h"

Son::Son(string name, Person* father, Person* mother):Children(name,father,mother){}

// Contrainte du garcon, ne peut pas rester seul avec sa mère si son père n'est pas la
bool Son::constraint(list<Person*> list, string* msg){
   bool motherPresent = false;
   bool fatherPresent = false;
   bool validate = true;
   for(Person* p : list){
      if(p == Children::getMother()){
         motherPresent = true;
      }else if(p == Children::getFather()){
         fatherPresent = true;
      }
   }
   
   if(motherPresent && !fatherPresent){
      validate = false;
      *msg += "\n### fils seul avec sa mere sans son pere";
   }
   
   return validate;
}