/* 
 * File:   Thief.h
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:05
 */

#ifndef THIEF_H
#define	THIEF_H
#include "Cop.h"
#include <string>

using namespace std;

/**
 * Classe représentant un voleur.
 */
class Thief : public Person{
   // Policier surveillant
   Cop* cop;
  
public:
   /**
    * Constructeur d'un voleur.
    * @param name nom du voleur
    * @param cop policier le surveillant
    */
   Thief(string name, Cop* cop);
   virtual ~Thief() {} 

   /**
    * Retourne faux
    * @return false
    */
   virtual bool canDrive();

   /**
    * Un voleur ne peut pas se retrouver avec des personnes si il n'y 
    * a pas le polcier avec.
    * @param list la liste où se trouve le voleur
    * @param msg le message d'erreur
    * @return false si le voleur se trouve avec des personnes sans le policier, 
    * true sinon
    */
   virtual bool constraint(list<Person*> list,string* msg);
};

#endif	/* THIEF_H */

