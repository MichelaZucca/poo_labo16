/* 
 * File:   Boat.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 5. mai 2017, 14:10
 */
#include "Boat.h"

/**
 * Constructor
 * @param name
 * @param current
 */
Boat::Boat(const string& name, Bank* current) : Container(name), _current(current) {}

/**
 * Ajouter une personne
 * @param p
 */
bool Boat::addPerson(Person* p) {
   if(placeAvailable()){
      numberPerson++;
      Container::addPerson(p);
      return true;
   }
   return false;
}

/**
 * Retirer une personne du bateau
 * @param p
 */
bool Boat::removePerson(Person* p) {
   if(numberPerson > 0){
      if(!Container::removePerson(p)){
         return false;
      }
      numberPerson--;
      return true;
   }
}

/**
 * Déplacer le bateau vers une rive
 * @param bank
 */
bool Boat::move(Bank* bank) {
   bool canDrive = false;
   for(Person* p : Container::getList()){
      if(p->canDrive()){
         _current = bank;
         return true;
      }
   }  
    return false;
}

/**
 * Retourne la rive actuelle sur laquelle se trouve le bateau
 * @return 
 */
Bank* Boat::getBank() {
    return _current;
}

void Boat::clear(Bank* bank){
   Container::clear();
   _current = bank;
   numberPerson=0;
}

bool Boat::placeAvailable() const{
   return numberPerson < maxPerson;
}

