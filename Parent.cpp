/* 
 * File:   Parent.cpp
 * Author: Mathieu Monteverde et Michela Zucca
 *
 * Created on 11. mai 2017, 14:04
 */
#include "Parent.h"

Parent::Parent(string name) : Person(name){}

bool Parent::canDrive(){
   return true;
}

bool Parent::constraint(list<Person*> list, string* msg){
   return true;
}